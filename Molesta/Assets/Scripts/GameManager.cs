using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public delegate void FinishedGameDel();
    public static FinishedGameDel puzzleFinishedDel;
    public static FinishedGameDel plattformerCompletedDel;

    public delegate void EnteredGame();
    public static EnteredGame enteredPlatformerGameDel;
    public static EnteredGame enteredPuzzleGameDel;

    public static bool inPlatformerGame = false;
    public static bool inPuzzleGame = false;

    public static bool platformerCompleted = false;
    public static bool puzzleCompleted = false;

    public PlayerMov playerMov;
    public Ball ball;

    public GameObject door;
    public GameObject Msj;

    public static Action MovePerson;

    private void Start()
    {
        Clock.TimeFinished += DesactivateDoor;

        puzzleFinishedDel += EnablePlayerMov;
        plattformerCompletedDel += EnablePlayerMov;

        enteredPlatformerGameDel += EnableBall;
        enteredPuzzleGameDel += DisablePlayerMov;

        enteredPlatformerGameDel += DisablePlayerMov;
    }
    public void DisablePlayerMov()
    {
        playerMov.enabled = false;
    }
    public void EnablePlayerMov()
    {
        playerMov.enabled = true;
    }
    public void EnableBall()
    {
       ball.enabled = true; 
    }
    public void DesactivateDoor()
    {
        door.SetActive(false);
        Msj.SetActive(true);
    }
}
