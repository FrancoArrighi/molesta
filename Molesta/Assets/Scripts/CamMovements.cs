using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMovements : MonoBehaviour
{
    [SerializeField] Transform currentView;
    [SerializeField] Transform[] views; // 0 player 1 tutorial 2 enterGame
    public float camSpeed;

    [SerializeField] GameObject player;

    private void Start()
    {
        currentView = transform;
        GameManager.puzzleFinishedDel += EnableCameraMov;
        GameManager.plattformerCompletedDel += EnableCameraMov;
    }

    private void Update()
    {
        if (GameManager.inPuzzleGame)
        {
            transform.parent = null;
            currentView = views[2];
            GetComponent<CameraMov>().enabled = false;
            return;
        }
        if (GameManager.inPlatformerGame);
        {
            transform.parent = null;
            currentView = views[1];
            GetComponent<CameraMov>().enabled = false;
            return;
        }
    }
    private void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, currentView.position, Time.deltaTime * camSpeed);
        Vector3 currentAngle = new Vector3(
            Mathf.Lerp(transform.rotation.eulerAngles.x, currentView.rotation.eulerAngles.x, Time.deltaTime * camSpeed),
            Mathf.Lerp(transform.rotation.eulerAngles.y, currentView.rotation.eulerAngles.y, Time.deltaTime * camSpeed),
            Mathf.Lerp(transform.rotation.eulerAngles.z, currentView.rotation.eulerAngles.z, Time.deltaTime * camSpeed)
            );
        transform.eulerAngles = currentAngle;
    }
    private void EnableCameraMov()
    {
        GetComponent<CameraMov>().enabled = true;
        transform.parent = player.transform;
        this.enabled = false;
    }
}
