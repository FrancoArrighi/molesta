using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    Rigidbody rb;
    public float vel;
    public float jumpForce;
   [SerializeField] bool isGroundeed = true;

    [SerializeField] Vector3 initialPos = new Vector3();

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");

        Vector3 movimiento = new Vector3(movimientoHorizontal * -1, 0f, 0f) * vel * Time.deltaTime;

        transform.Translate(movimiento);

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            Jump();
        }

        if(transform.localPosition.y < 1f) 
        {
            transform.localPosition = initialPos;
        }
    }
    private void Jump()
    {
        if (isGroundeed)
        {
            rb.AddForce(Vector3.up * jumpForce);
            isGroundeed = false;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("floor"))
        {
            isGroundeed = true;
        }
        if (collision.gameObject.CompareTag("win"))
        {
            GameManager.plattformerCompletedDel();
            GameManager.platformerCompleted = true;
            this.transform.parent.gameObject.SetActive(false);
        }
    }
}

