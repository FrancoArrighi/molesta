using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMov : MonoBehaviour
{
    public float vel;
    Rigidbody rb;

    bool nearPuzzle = false;
    bool nearPlatformer = false;

    public GameObject interactionImg;
    public GameObject wallGame;
    public GameObject puzzleGame;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        float movVertical = Input.GetAxisRaw("Vertical");
        float movHorizontal = Input.GetAxisRaw("Horizontal");

        Vector3 velocity = Vector3.zero;
        if (movVertical != 0 || movHorizontal != 0)
        {
            Vector3 dir = (transform.forward * movVertical + transform.right * movHorizontal).normalized;
            velocity = dir * vel;
        }
        rb.velocity = velocity;

        if (Input.GetMouseButtonDown(0))
        {
            if (nearPuzzle && !GameManager.inPuzzleGame)
            {
                interactionImg.SetActive(false);
                GameManager.inPuzzleGame = true;
                GameManager.enteredPuzzleGameDel();
            }
            if (nearPlatformer && !GameManager.inPlatformerGame)
            {
                interactionImg.SetActive(false);
                GameManager.inPlatformerGame = true;
                GameManager.enteredPlatformerGameDel();
            }
        }
        if (transform.position.x <= -8.5f)
        {
            Fade.instance.ChangeScene(0,0);
            // hacer fade de que termine el juego y arranque de nuevo 
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("puzzleGame"))
        {
            if (!GameManager.puzzleCompleted)
            {
                interactionImg.SetActive(true);
                puzzleGame.SetActive(true);
                nearPuzzle = true;
            }
        }
        if (other.gameObject.CompareTag("platformerGame"))
        {
            if (!GameManager.platformerCompleted)
            {
                interactionImg.SetActive(true);
                wallGame.SetActive(true);
                nearPlatformer = true;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("puzzleGame"))
        {
            nearPuzzle = false;
            puzzleGame.SetActive(false);
            interactionImg.SetActive(false);
        }
        if (other.gameObject.CompareTag("platformerGame"))
        {
            nearPlatformer = false;
            wallGame.SetActive(false);
            interactionImg.SetActive(false);
        }
    }
}
