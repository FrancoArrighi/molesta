using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMov : MonoBehaviour
{
    Vector2 mousePos;
    Vector2 suavidadV;

    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;

    public Transform posCam;
    [SerializeField]
    GameObject player;


    private void Start()
    {
        Cursor.visible = true;
        SetFirstPersonCursor();
        GameManager.enteredPuzzleGameDel += DisableCameraMov;
        GameManager.enteredPlatformerGameDel += DisableCameraMov;
    }
    private void OnEnable()
    {
        SetFirstPersonCursor();
    }

    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));

        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado);
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);

        mousePos += suavidadV;
        mousePos.y = Mathf.Clamp(mousePos.y, -90f, 90f);

        transform.localRotation = Quaternion.AngleAxis(-mousePos.y, Vector3.right);
        player.transform.localRotation = Quaternion.AngleAxis(mousePos.x, player.transform.up);

    }
    public void SetFirstPersonCursor()
    {
        this.transform.parent = player.transform;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        transform.localPosition = posCam.localPosition;
    }
    private void DisableCameraMov()
    {
        GetComponent<CamMovements>().enabled = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        this.enabled = false;
    }
}
