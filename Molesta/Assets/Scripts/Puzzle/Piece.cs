using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;

public class Piece : MonoBehaviour
{
    private Vector3 offset;
    private bool isDragging = false;

    private SpriteRenderer spriteRenderer;
    public Color[] colors;
    private SortingGroup sortingGroup;

    Vector3 rightPos;
    public bool inRightPos;
    int sortingOrder = 0;
    float distance = 1.45f;

    private void Start()
    {
        rightPos = transform.position;

        sortingGroup = GetComponent<SortingGroup>();
        spriteRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }
    private void OnMouseDown()
    {
        if (inRightPos) { return; }
        sortingGroup.sortingOrder = sortingOrder++;
        PuzzleManager.draggingPiece = true;
        isDragging = true;
    }

    private void OnMouseDrag()
    {
       
        Vector3 mousePosition;
        if (isDragging)
        {
            mousePosition = Input.mousePosition;
            mousePosition.z = distance;
            transform.position = Camera.main.ScreenToWorldPoint(mousePosition);
        }
    }

    private void OnMouseUp()
    {
        CheckPiecePosition();
        isDragging = false;
        PuzzleManager.draggingPiece = false;
    }

    private void OnMouseEnter()
    {
        if (!PuzzleManager.draggingPiece && !inRightPos && GameManager.inPuzzleGame)
            spriteRenderer.color = colors[1];
    }
    private void OnMouseExit()
    {
        spriteRenderer.color = colors[0];
    }

    public void CheckPiecePosition()
    {
        if (Vector3.Distance(transform.position, rightPos) < 0.1f)
        {
            transform.position = rightPos;
            inRightPos = true;
            sortingGroup.sortingOrder = 0;
            PuzzleManager.pieceInRightPlace();
        }
    }
    public void SetRandomPos() // llamarlo cuando entre al puzzle
    {
        Vector3 newPos = new Vector3(Random.Range(-1.35f, -0.25f),0, Random.Range(-1.65f, -0.55f));
        this.transform.localPosition = newPos;
    }
}
