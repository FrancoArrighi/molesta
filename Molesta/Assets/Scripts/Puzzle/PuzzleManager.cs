using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    public static bool draggingPiece = false;

    public List<Piece> pieceList = new List<Piece>();

    public static Action pieceInRightPlace { get; set; }

    void Start()
    {
        GameManager.enteredPuzzleGameDel += EnablePieces;
        pieceInRightPlace += PuzzleCompleted;
    }

    public void PuzzleCompleted()
    {
        foreach (Piece piece in pieceList)
        {
            if (!piece.inRightPos)
            {
                return;
            }
        }
        GameManager.puzzleCompleted = true;
        this.gameObject.SetActive(false);
        GameManager.puzzleFinishedDel();
    }

    public void EnablePieces()
    {
        Invoke("SetPieces", 1f);
    }
    private void SetPieces()
    {
        foreach (Piece piece in pieceList)
        {
            piece.enabled = true;
            piece.SetRandomPos();
        }
    }
}
