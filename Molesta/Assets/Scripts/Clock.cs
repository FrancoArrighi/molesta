using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Clock : MonoBehaviour
{
    public Action MovePerson;
    public static Action TimeFinished;

    public float StartTime = 900f;
    public float time = 900f;
    bool timeRunning = true;
    int cont = 6;
    float notificationTime = 0;
    [SerializeField] TMP_Text timer;

    void Start()
    {
        time = StartTime;
        notificationTime = StartTime / cont;
        GameManager.puzzleFinishedDel += ReduceTime;
        GameManager.plattformerCompletedDel += ReduceTime;
    }

    private void Update()
    {
        if (timeRunning)
        {
            if (time > 0)
            {
                time -= Time.deltaTime;


                int minutos = Mathf.FloorToInt(time / 60f);
                int segundos = Mathf.FloorToInt(time % 60f);
                timer.text = minutos.ToString("00") + ":" + segundos.ToString("00");

                if (time <= notificationTime * cont)
                {
                    GameManager.MovePerson();
                    cont--;
                }
            }
            else
            {
                timer.text = "0";
                TimeFinished();
                timeRunning = false;
            }
        }
    }

    public void ReduceTime()
    {
        time -= notificationTime;
    }
}
