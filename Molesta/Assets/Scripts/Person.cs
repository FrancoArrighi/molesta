using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Person : MonoBehaviour
{
    NavMeshAgent agent;
    [SerializeField] Transform finalPos;
    public bool ready = false;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    public void MoveToFinalPos()
    {
        ready = true;
        Debug.Log("asdasdas");
    }
    private void Update()
    {
        if (ready)
            agent.SetDestination(finalPos.position);

        if (transform.position == finalPos.position)
        {
            this.gameObject.SetActive(false);
        }
    }
}
