using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleManager : MonoBehaviour
{
    [SerializeField] List<Person> people = new List<Person>();
    int cont = 0;
    void Start()
    {
        GameManager.MovePerson += MoveNextPerson;
    }

    private void MoveNextPerson()
    {
        if (cont < people.Count)
        {
            people[cont].MoveToFinalPos();
            cont++;
        }
    }
}
