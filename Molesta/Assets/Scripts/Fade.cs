using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Fade : MonoBehaviour
{
    private Animator transition;


    public static Fade instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        transition = GetComponent<Animator>();
    }
    public void ChangeScene(int index, float seconds)
    {
        StartCoroutine(StartTransition(index, seconds));
    }

    IEnumerator StartTransition(int index, float seconds)
    {
        yield return new WaitForSeconds(seconds);

        transition.SetTrigger("Start");

        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
